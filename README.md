![Pipeline status](https://gitlab.com/roza/latex-pipeline/badges/master/pipeline.svg)

# Gitlab CI pipeline for LaTeX example

This repository contains examples for the blog post [How to annoy your co-authors: a Gitlab CI pipeline for LaTeX](https://blog.martisak.se/2020/05/11/gitlab-ci-latex-pipeline/).

Compile locally with 

`make clean render`

or 

`make clean render LATEXMK_OPTIONS_EXTRA=-pvc` to keep compiling the pdf when the input files are updated.


[![PDF](https://img.shields.io/badge/Download-PDF-green)](https://gitlab.com/roza/latex-pipeline/-/jobs/artifacts/master/raw/main.pdf?job=compile)